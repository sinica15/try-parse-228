const request = require("sync-request");
const fs = require("fs");
const cheerio = require("cheerio");

var URL;
URL = 'https://www.kufar.by/минск_город/Компьютеры'; 
//URL = 'https://www.kufar.by/беларусь/?cu=BYR&o=2'; 
console.log("\n");

if (!fs.existsSync("pages")){
    fs.mkdirSync("pages");
}
fs.writeFileSync("pages/ads.txt", "" , 'utf-8');
console.log("clear file finished");

fs.writeFileSync("pages/link_temp.txt", URL, 'utf-8');
console.log("first URL recorded");

fs.writeFileSync("pages/used_links.txt", URL + "\n", 'utf-8');
console.log("create file used_links");

var results = [];

for (var i = 0; i < 10; i++){
    if (i >= 10) { console.log("\n------------" + i + "-----------"); }
    else { console.log("\n------------0" + i + "-----------"); }
	var readURL = fs.readFileSync("pages/link_temp.txt", "utf8");
	console.log("URL read " + "|" + readURL + "|");
	
	var uncodedReadURL = encodeURI(readURL); 
	
	console.log("|" + uncodedReadURL + "|");
	
	var req_res = request('GET', uncodedReadURL);
	var body = req_res.body.toString('utf-8');
    

    var $ = cheerio.load(body);
    
    var ads = $(".list_ads__info_container");
    ads.each(function (i, ad) {
        results.push({
        	name: $(ad).children(".list_ads__title").text(),
        	href: $(ad).children(".list_ads__title").attr("href"),
            price: $(ad).children(".list_ads__price").text(),
            date_time: $(ad).children(".list_ads__date_location").children(".list_ads__date").attr("datetime"),
            location: $(ad).children(".list_ads__date_location").children(".list_ads__location").text(),
            type: $(ad).children(".list_ads__params_block").children("li:first-of-type").text(),
            state: $(ad).children(".list_ads__params_block").children("li:last-of-type").text()
      });
    });


    var searchNextUrl = $("a.pagination__item");
    searchNextUrl.each(function(i, url_i){
		if($(url_i).text() == "След. "){
			fs.writeFileSync("pages/link_temp.txt", $(url_i).attr("href"), 'utf-8');
			fs.appendFileSync("pages/used_links.txt", $(url_i).attr("href") + "\n", 'utf-8');
			console.log("some URL recorded" + "(" + $(url_i).attr("href") + ")");
		 }
    });  
    
    fs.appendFileSync("pages/ads.txt", JSON.stringify(results, null, 2) , 'utf-8');
	console.log("writing some page finished");
    
    var ad = fs.readFileSync("pages/ads.txt", "utf8");
    ad = ad.replace(/\n]\[/gi,',');
    ad = ad.replace(/(\\n\s{0,})/gi,'');
    ad = ad.replace(/\B\\n/gi,'');
    ad = ad.replace(/\\t/gi,'');
    fs.writeFileSync("pages/ads.txt", ad, 'utf-8');
    
}
