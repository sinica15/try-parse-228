var tress = require('tress');
var needle = require('needle');
var cheerio = require('cheerio');
var resolve = require('url').resolve;
var fs = require('fs');

var URL = 'https://www.kufar.by/';
var results = [];

if (!fs.existsSync("pages")){
    fs.mkdirSync("pages");
}
fs.writeFile("pages/ads.txt", "" , 'utf-8', function(error){
	if(error) throw error;
	console.log("clear file finished");
});

var q = tress(function(url, callback){
	console.log("жопа");
    needle.get(url, function(err, res){
        if (err) throw err;

        // парсим DOM
        var $ = cheerio.load(res.body);

        var ads = $("a.list_ads__title");

		ads.each(function (i, ad) {
			results.push({
				name: $(ad).text(),
				href: $(ad).attr("href")
			});
		});
		
		var searchNextUrl = $("a.pagination__item");
    	searchNextUrl.each(function(i, url_i){
      		if($(url_i).text() == "След. "){ q.push($(url_i).attr("href")); }
    	});

        callback();
    });
}, 4); // запускаем 4 параллельных потоков

console.log(results);

q.drain = function(){
    fs.appendFile("pages/ads.txt", JSON.stringify(results, null, 2) , 'utf-8', function(error){
    	if(error) throw error;
    	console.log("writing page finished");
    });
}

q.push(URL);